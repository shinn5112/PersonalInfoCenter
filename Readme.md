# Raspberry Pi Info Center
[![Build Status](https://travis-ci.org/shinn16/PersonalInfoCenter.png?branch=master)](https://travis-ci.org/shinn16/PersonalInfoCenter)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/shinn16/PersonalInfoCenter/pulls)
[![License](https://img.shields.io/badge/license-GPLV3-blue.svg)](https://github.com/shinn16/PersonalInfoCenter/blob/master/LICENSE.txt)

Java implementation of a personal information center via Raspberry Pi.

#### State: In development

### Features
* Google Calendar Integration
* DarkSky Weather Forecast

---

### Usage
Although this project was intended to be used on a Raspberry Pi, you can run it
without one if you do so choose. All you need to have installed to run is the 
[Java Runtime Environment (JRE)](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html), a [Google Account](https://myaccount.google.com/?pli=1),
and an Internet connection.

##### Raspberry Pi Setup
Instructions comming soon!

---

### Development
Build System: [Gradle](https://gradle.org/)

API Usage:
* [Google Calendar API](https://developers.google.com/google-apps/calendar/)
* [Google Maps API](https://developers.google.com/maps/)
* [Dark Sky Java API](https://github.com/200Puls/darksky-forecast-api)

In order to develop for this project, you will need a [DarkSky API Key](https://darksky.net/dev/)
and a [Google Account](https://myaccount.google.com/?pli=1). You will need to enable Google Developer Console for your
account, to do this, please refer to the Google Calendar API link above. You will also need to enable the Google Maps
Geocoding API. This is used to convert a zipcode into a usable latitude and longitude for the DarkSky API. With all of the 
Google APIs in mind, please note that you will also need the respective API keys and client secret files.
