import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Calendar;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.*;
import tk.plogitech.darksky.forecast.model.Forecast;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/***
 * Controller Class
 *
 * @author Patrick Shinn
 * @version 5/10/17
 */

public class Controller {
    // Google calendar stuff
    private static final String APPLICATION_NAME = "Personal Info Center";
    private static final java.io.File DATA_STORE_DIR = new
            java.io.File(System.getProperty("user.home"), ".credentials/Personal Info Center");
    private static FileDataStoreFactory DATA_STORE_FACTORY;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static HttpTransport HTTP_TRANSPORT;
    private static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {;
            t.printStackTrace();
        }
    }

    // other variables
    private com.google.api.services.calendar.Calendar calendarService;
    private String DarkSkyKey;
    private String GoogleMapsKey;
    private Forecast forecast;
    private String zipcode;


    /**
     * Initializes the application framework before the GUI pops up.
     */
    public void initialize(){

       //todo Differentiate between first run and others.
        try{
            // getting various API keys
            Scanner scanner = new Scanner(new File("secrets/DarkSky.key"));
            DarkSkyKey = scanner.nextLine();

            scanner = new Scanner(new File("secrets/GoogleMapsAPI.key"));
            GoogleMapsKey = scanner.nextLine();

            scanner.close();

            // getting weather information
            zipcode = "25701"; //todo make the user supply this
            forecast = getWeather(getLocation(zipcode));

            // getting Google Calendar // todo oauth does not open in browser, fix it.
            calendarService = getCalendarService();
            com.google.api.services.calendar.Calendar.Events events = calendarService.events();
            System.out.println(events.toString());
        }catch (Exception e){
            e.printStackTrace();
            System.exit(0);
        }
    }


    /**
     * Gets the weather of a given latitude and longitude using the DarkSky API
     *
     * @param location LatLng object containing latitude and longitude for desired location
     * @return Forecast object for the given location
     * @throws ForecastException required by the API
     */
    private Forecast getWeather(LatLng location) throws ForecastException{
        ForecastRequest request = new ForecastRequestBuilder()
                .key(new APIKey(DarkSkyKey)) // supplying api key
                .location(new GeoCoordinates(new Longitude(location.lng), new Latitude(location.lat))) // giving location
                .language(ForecastRequestBuilder.Language.en) // setting language for output
                .units(ForecastRequestBuilder.Units.us) // setting units
                .exclude(ForecastRequestBuilder.Block.minutely) // excluding minutely forecast
                .build();

        return new DarkSkyJacksonClient().forecast(request);
    }

    /**
     * Returns the Latitude and Longitude of a given zipcode using the Google Maps API
     *
     * @param zipcode of desired location
     * @return LatLng object containing latitude and longitude.
     * @throws IOException required by API
     * @throws ApiException required by API
     * @throws InterruptedException required by API
     */
    private LatLng getLocation(String zipcode) throws IOException, ApiException, InterruptedException{
        GeoApiContext context = new GeoApiContext().setApiKey(GoogleMapsKey);
        GeocodingResult[] results =  GeocodingApi.geocode(context, "25701").await();
        return results[0].geometry.location;
    }

    /**
     * Creates an authorized Credential object.
     * @return an authorized Credential object.
     * @throws IOException Required
     */
    private static Credential authorize() throws IOException {
        // Load client secrets.
        InputStream in = new FileInputStream(new File("secrets/client_secret.json"));
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("user");
        System.out.println(
                "Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }

    /**
     * Build and return an authorized Calendar client service.
     * @return an authorized Calendar client service
     * @throws IOException Required
     */
    private static com.google.api.services.calendar.Calendar
    getCalendarService() throws IOException {
        Credential credential = authorize();
        return new com.google.api.services.calendar.Calendar.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }
}
